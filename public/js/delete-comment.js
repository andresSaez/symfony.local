function iniciarEventos()
{
    let arrDeletes = document.querySelectorAll('.deleteC');

    arrDeletes.forEach(element => {
        element.addEventListener('click', deleteComment);
    });
}

function deleteComment(event)
{
    event.preventDefault();

    let element = event.target;

    let id = element.getAttribute('id');

    let url = 'http://dwes.local/delete-comment?id=' + id;

    fetch(url, {
        method: 'GET',
        headers: {},
        body: null
    }).then(response => {
        if (response.ok)
        {
            element.parentElement.parentElement.remove();
        }
        else
        {
            alert('No se ha podido borrar el elemento');
        }
    })
}

document.addEventListener("DOMContentLoaded", iniciarEventos);
