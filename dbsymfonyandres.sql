-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 12-02-2019 a las 11:52:19
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbsymfonyandres`
--
CREATE DATABASE IF NOT EXISTS `dbsymfonyandres` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `dbsymfonyandres`;

--
-- Usuario: 'andres'
--
CREATE USER 'andres'@'localhost' IDENTIFIED BY 'dwes'; 
GRANT USAGE ON *.* TO 'andres'@'localhost' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT SELECT, INSERT, UPDATE, DELETE ON `dbsymfonyandres`.* TO 'andres'@'localhost';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `booking`
--

DROP TABLE IF EXISTS `booking`;
CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `check_in` datetime NOT NULL,
  `check_out` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_USER_BOOKING` (`user_id`),
  KEY `FK_HOTEL_BOOKING` (`hotel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `booking`
--

INSERT INTO `booking` (`id`, `hotel_id`, `user_id`, `check_in`, `check_out`) VALUES
(2, 7, 4, '2018-12-06 00:00:00', '2018-12-06 00:00:00'),
(4, 4, 6, '2018-12-15 00:00:00', '2018-12-20 00:00:00'),
(5, 3, 1, '2019-01-28 00:00:00', '2019-01-28 00:00:00'),
(6, 3, 1, '2019-01-28 00:00:00', '2019-01-29 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `text` varchar(1000) COLLATE utf8_spanish2_ci NOT NULL,
  `stars` smallint(6) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_USER_COMMENT` (`user_id`),
  KEY `FK_HOTEL_COMMENT` (`hotel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `comment`
--

INSERT INTO `comment` (`id`, `user_id`, `hotel_id`, `text`, `stars`, `date`) VALUES
(4, 1, 7, 'Este hotel no me gusta', 3, '2018-12-06 12:58:07'),
(6, 1, 4, 'Este hotel se merece un 3', 3, '2018-12-10 10:13:44'),
(7, 1, 12, 'Nuevo comentario', 2, '2018-12-19 17:28:34'),
(11, 15, 5, 'Me gustó mucho la piscina', 4, '2018-12-23 20:50:54'),
(12, 15, 13, 'La comida sabe un poco fuerte', 3, '2018-12-23 20:52:13'),
(13, 1, 9, 'Es carísimo', 5, '2018-12-23 21:02:54'),
(14, 1, 9, 'Aunque la cama era muy cómoda', 4, '2018-12-23 21:05:00'),
(15, 1, 9, 'Voy a probar otro comentario', 4, '2018-12-23 21:07:14'),
(16, 1, 13, 'Voy a probar el sistema de puntuaciones', 5, '2018-12-23 21:13:28');

--
-- Disparadores `comment`
--
DROP TRIGGER IF EXISTS `UPDATE_HOTEL_STARS`;
DELIMITER $$
CREATE TRIGGER `UPDATE_HOTEL_STARS` AFTER INSERT ON `comment` FOR EACH ROW UPDATE hotel SET stars = (
    SELECT AVG(stars) FROM `comment` 
    WHERE `comment`.hotel_id = NEW.hotel_id
) WHERE id = NEW.hotel_id
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotel`
--

DROP TABLE IF EXISTS `hotel`;
CREATE TABLE IF NOT EXISTS `hotel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img_name` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `hotel_name` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `stars` decimal(4,2) NOT NULL DEFAULT '0.00',
  `swimming_pool` tinyint(1) NOT NULL,
  `gym` tinyint(1) NOT NULL,
  `wifi` tinyint(1) NOT NULL,
  `room_service` tinyint(1) NOT NULL,
  `air_condition` tinyint(1) NOT NULL,
  `restaurant` tinyint(1) NOT NULL,
  `price` decimal(4,2) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_USER_HOTEL` (`creator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `hotel`
--

INSERT INTO `hotel` (`id`, `img_name`, `hotel_name`, `description`, `stars`, `swimming_pool`, `gym`, `wifi`, `room_service`, `air_condition`, `restaurant`, `price`, `creator_id`, `date`, `active`) VALUES
(3, '1545591870barcelo.jpg', 'Hotel Barceló', 'El Grupo Barceló, fundado en 1931 por Simón Barceló en la isla de Mallorca (España), es un grupo turístico verticalmente integrado que está formado por la división hotelera Barceló Hotel Group, que cuenta con más de 245 hoteles y más de 54.000 habitaciones en 22 países; y por la división de viajes Ávoris, que dispone de más de 700 agencias de viajes en 4 continentes, varios touroperadores y receptivos, e incluso una compañía aérea.', '1.00', 0, 0, 1, 1, 1, 0, '54.00', 1, '2018-12-23', 1),
(4, '1545592079castillatermal.jpg', 'Hotel Castilla Termal', 'Nace con el fin de poner en marcha una red de hoteles termales en lugares emblemáticos de Castilla y León y Cantabria, con un elemento diferenciador sin precedente en la historia de la hotelería nacional, la combinación de edificios históricos y aguas termales. Las propiedades particulares de las aguas de cada establecimiento, unido a una arquitectura única y a la cultura, historia y el encanto de estos lugares, lo convierten en una oferta única y saludable para disfrutar de un tiempo de descanso y bienestar.', '5.00', 1, 1, 1, 1, 0, 0, '23.00', 1, '2018-12-23', 1),
(5, '1545592174daoura.jpg', 'Luna Hotel Da Oura', 'Situado a 2 minutos andando de la bulliciosa zona del Strip de Albufeira, el Luna Hotel da Oura es la elección ideal para aquellos que desean disfrutar de sus vacaciones. La diversidad de servicios complementarios de entretenimiento y bienestar, hacen de éste, un hotel para toda la familia.', '5.00', 1, 0, 0, 1, 0, 1, '10.00', 1, '2018-12-23', 1),
(7, '1545592281granmelia.jpg', 'Gran Meliá', 'Gran Meliá Hotels &amp; Resorts son los mejores hoteles dentro de la oferta hotelera de Meliá Hotels International y constituyen el concepto definitivo del lujo contemporáneo. Su estilo, diseño y ubicación hacen que sean los hoteles predilectos por los clientes más exigentes.', '2.00', 1, 1, 1, 1, 1, 1, '99.99', 1, '2018-12-23', 1),
(9, '1545592435hardrock.jpg', 'Hard Rock Ibiza', 'Las elegantes y modernas habitaciones cuentan con conexión Wi-Fi gratuita y televisión de pantalla plana con canales por satélite, además de cortinas opacas. También disponen de escritorio y terraza. Las suites tienen sala de estar y jacuzzi exterior. Las suites superiores cuentan con comedor, vistas al mar y camas redondas. Se ofrece servicio de habitaciones.', '2.00', 1, 0, 1, 1, 1, 1, '99.99', 5, '2018-12-23', 1),
(10, '1545592524nacionalcuba.jpg', 'Hotel Nacional de Cuba', 'Ubicado en el saliente costero de Punta Brava, en la loma de Taganana casi al extremo de la caleta de San Lázaro, sitio habitual de desembarcos de piratas, se alza el Hotel Nacional de Cuba desde el 30 de diciembre de 1930, como el más importante del Gran Caribe.', '0.00', 0, 1, 0, 1, 0, 1, '12.00', 6, '2018-12-23', 1),
(12, '1545592635naxos.jpg', 'Hotel Anixis', 'El hotel Anixis, situado en el casco antiguo de Naxos, a 400 metros de la playa Agios Georgios, ofrece conexión Wi-Fi gratuita. Las habitaciones disponen de balcón con vistas al mar Egeo o al castillo.  Las habitaciones del hotel de estilo cicládico Anixis disponen de muebles de madera oscura y de mosquiteras. Todas incluyen TV vía satélite, radio y nevera.', '2.00', 0, 0, 0, 0, 1, 1, '35.00', 4, '2018-12-23', 1),
(13, '1545592725riupanama.jpg', 'Hotel Riu Plaza Panamá', 'El Hotel Riu Plaza Panama te ofrece más de 600 habitaciones distribuidas en 34 plantas con todos los servicios imprescindibles para obtener el máximo confort. Podrás disfrutar de WiFi gratuito, TV satélite, mini nevera, sala de estar y máquina de café, entre otros servicios. Además, los clientes que se hospeden en habitaciones ejecutivas y suites, tendrán numerosos servicios gratuitos como área privada de check-in y check-out, sala de reuniones, servicio de conserje y descuentos en nuestro Renova Spa.', '4.00', 1, 0, 1, 1, 1, 1, '68.00', 4, '2018-12-23', 1),
(15, '1545592809tukan.jpg', 'Hotel Tukan', '¡En Tukan Hotels, te invitamos a vivir la experiencia de descubrir la Riviera Maya en nuestros hoteles ubicados en el corazón de Playa del Carmen! Conoce y disfruta de la Quinta Avenida, con la mejor gastronomía local e internacional. ¡Tukan Hotels te ofrece el confort y la calidez que te mereces para tus vacaciones!', '0.00', 1, 1, 0, 1, 0, 1, '90.00', 4, '2018-12-23', 1),
(16, '1545592962scala.jpg', 'Hotel Scala', 'Orientado a brindar un trato preferencial, Hotel Scala ofrece calificadas prestaciones en un marco arquitectónico que otorga personalidad y estilo. Un verdadero privilegio rodeado de cordialidad y excelente servicio. Un pintoresco paisaje, con vistas al valle de Rosalía de Castro y a minutos del campo de golf.', '0.00', 0, 0, 0, 1, 1, 1, '15.00', 6, '2018-12-23', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensaje`
--

DROP TABLE IF EXISTS `mensaje`;
CREATE TABLE IF NOT EXISTS `mensaje` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remitente_id` int(11) NOT NULL,
  `destinatario_id` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `texto` text COLLATE utf8_spanish2_ci NOT NULL,
  `respuestaa` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_USER_DEST` (`destinatario_id`),
  KEY `FK_USER_REMIT` (`remitente_id`),
  KEY `FK_RESPUESTAA` (`respuestaa`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `mensaje`
--

INSERT INTO `mensaje` (`id`, `remitente_id`, `destinatario_id`, `titulo`, `texto`, `respuestaa`, `date`) VALUES
(1, 4, 1, 'Mensaje 1', 'Hola este es mi primer mensaje', NULL, '2018-12-06 10:46:56'),
(4, 4, 1, 'Mensaje 4', 'Prueba de mensaje desde la página de mensajes', NULL, '2018-12-06 18:10:17'),
(5, 4, 1, 'Mensaje de prueba 5', 'Este es un mensaje de prueba después de aplicar el respond-to', NULL, '2018-12-06 18:19:53'),
(6, 4, 1, 'Mensaje 1', 'jhj', NULL, '2018-12-19 17:21:29'),
(10, 12, 4, 'Prueba avatar redimensionado', 'Mensaje de prueba 9', NULL, '2018-12-23 13:28:05'),
(11, 15, 4, 'Mensaje 3', 'pooooooo', NULL, '2018-12-23 13:59:32'),
(12, 15, 1, 'Duda reserva', 'Hola, querría saber si aceptan mascotas', NULL, '2018-12-23 20:51:37'),
(13, 15, 4, 'comida vegana', 'Hola, tienen comida vegana?', NULL, '2018-12-23 20:52:47'),
(14, 4, 4, 'Mensaje en Symfony', 'Primer mensaje probando symfony', 1, '2019-02-02 14:55:07'),
(15, 4, 4, 'No me gusta tu hoteñ', 'bla bla bla', 1, '2019-02-10 19:54:34'),
(16, 4, 4, 'jajaja', 'jajajajajas', 1, '2019-02-10 19:55:05'),
(17, 4, 15, 'otro mas', 'asdasdads', 1, '2019-02-10 19:55:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nameuser` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `province` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `avatar` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'profile.jpg',
  `email` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'ADMIN',
  `language` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nameuser`, `surname`, `province`, `avatar`, `email`, `password`, `role`, `language`) VALUES
(1, 'andres', 'Saez', 'Alicante', '1545595053user10.png', 'andres@gmail.com', '$2y$10$RKfrtjO1JUsmwDD1BGPUGO8SI7zaNuF7QHTTgx16MRXRp/OmXqO.K', 'ROLE_ADMIN', 'español'),
(4, 'Daniel', 'Lopez', 'Madrid', '1545593597user5.jpg', 'dani@gmail.com', '$2y$10$VSfXvaDh18ZUse9f3XmQwOb.HdgAJGbjBP0uCcKkZpa5RxHMCZe5S', 'ROLE_USER', 'english'),
(5, 'Pablo', 'Romero', 'Valencia', '1545594229user6.jpg', 'pablo@gmail.com', '$2y$10$W6XSXrLt9WK4fjSYVqhNKebJHV2cp./qh5tbrh.iIRDHUqmhmPhO6', 'ROLE_USER', 'español'),
(6, 'admin', 'admin', 'Alicante', '1545593512user9.png', 'admin@gmail.com', '$2y$10$RKfrtjO1JUsmwDD1BGPUGO8SI7zaNuF7QHTTgx16MRXRp/OmXqO.K', 'ROLE_ADMIN', 'english'),
(8, 'Ivan', 'Simpson', 'Barcelona', '1545594267user7.jpg', 'ivan@gmail.com', '$2y$10$hgHios04eAaj8l8WbipPfO7UdWurj1fxxZQ16/j/cJgGhIPclE0fK', 'ROLE_USER', 'english'),
(12, 'Maria', 'Sanchex', 'Barcelona', '1545594325user1.jpg', 'maria@gmail.com', '$2y$10$2UN3g.ovREthm/4SHEizouzpPuuoeCMwlzchUS3k5SEU.seVmLYYW', 'ROLE_USER', 'english'),
(15, 'Laura', 'Simpson', 'Toledo', '1545594349user3.jpg', 'laura@gmail.com', '$2y$10$c0jHiO8GzFxIddtnQIliCe6qQt7w2PzevxNDhKhyTHAjSfmxO3z6K', 'ROLE_USER', 'español');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `FK_HOTEL_BOOKING` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  ADD CONSTRAINT `FK_USER_BOOKING` FOREIGN KEY (`user_id`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_HOTEL_COMMENT` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_USER_COMMENT` FOREIGN KEY (`user_id`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `hotel`
--
ALTER TABLE `hotel`
  ADD CONSTRAINT `FK_USER_HOTEL` FOREIGN KEY (`creator_id`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `mensaje`
--
ALTER TABLE `mensaje`
  ADD CONSTRAINT `FK_RESPUESTAA` FOREIGN KEY (`respuestaa`) REFERENCES `mensaje` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_USER_DEST` FOREIGN KEY (`destinatario_id`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `FK_USER_REMIT` FOREIGN KEY (`remitente_id`) REFERENCES `usuario` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
