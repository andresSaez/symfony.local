<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 8/02/19
 * Time: 22:22
 */

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class SecurityController extends AbstractController
{
    /**
    * @Route("/login", name="login")
    * @Template("security/login.html.twig")
    */
    public function login(AuthenticationUtils $authUtils)
    {
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return [
            'last_username' => $lastUsername,
            'error' => $error
       ];
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
    }

}