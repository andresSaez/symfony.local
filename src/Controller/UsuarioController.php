<?php

namespace App\Controller;

use App\Entity\Usuario;
use App\Form\UsuarioType;
use App\Form\UsuarioUpdatePasswordType;
use App\Repository\UsuarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Helper\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/usuario")
 */
class UsuarioController extends AbstractController
{
    /**
     * @Route("/", name="usuario_index", methods={"GET"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function index(UsuarioRepository $usuarioRepository): Response
    {
        return $this->render('usuario/index.html.twig', [
            'usuarios' => $usuarioRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="register", methods={"GET","POST"})
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $usuario = new Usuario();
        $form = $this->createForm(UsuarioType::class, $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $avatar */
            $avatar = $usuario->getAvatar();
            $fileName = $fileUploader->upload($avatar);
            $usuario->setAvatar($fileName);

            $opciones = ['cost' => 12];
            $pass = $usuario->getPassword();
            $passEncriptado = password_hash($pass, PASSWORD_BCRYPT, $opciones);
            $usuario->setPassword($passEncriptado);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($usuario);
            $entityManager->flush();

            return $this->redirectToRoute('login');
        }

        return $this->render('usuario/new.html.twig', [
            'usuario' => $usuario,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/buscar", name="usuario_index_buscar", methods={"POST"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function buscar(Request $request, UsuarioRepository $usuarioRepository): Response
    {
        $filterByRole = $request->request->get('filterByRole');

        if ($filterByRole != 'TODOS')
            $usuarios = $usuarioRepository->findBy(['role' => $filterByRole]);
        else
            $usuarios = $usuarioRepository->findAll();

        return $this->render('usuario/index.html.twig', [
            'usuarios' => $usuarios,
        ]);
    }

    /**
     * @Route("/{id}", name="usuario_show", methods={"GET"})
     * @Security("has_role('ROLE_USER')")
     */
    public function show(Usuario $usuario): Response
    {
        return $this->render('usuario/show.html.twig', [
            'usuario' => $usuario,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="usuario_edit", methods={"GET","POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function edit(Request $request, Usuario $usuario, FileUploader $fileUploader): Response
    {
        if (($this->getUser() === $usuario) || ($this->getUser()->getRole() === 'ROLE_ADMIN')) {
            $usuario->setAvatar(new File(
                $this->getParameter('images_directory') . '/' . $usuario->getAvatar()));

            $form = $this->createForm(UsuarioType::class, $usuario);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                /** @var UploadedFile $avatar */
                $avatar = $usuario->getAvatar();
                $fileName = $fileUploader->upload($avatar);
                $usuario->setAvatar($fileName);

                $opciones = ['cost' => 12];
                $pass = $usuario->getPassword();
                $passEncriptado = password_hash($pass, PASSWORD_BCRYPT, $opciones);
                $usuario->setPassword($passEncriptado);

                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('usuario_show', [
                    'id' => $usuario->getId(),
                ]);
            }

            return $this->render('usuario/edit.html.twig', [
                'usuario' => $usuario,
                'form' => $form->createView(),
            ]);
        }
        else {
            throw new AccessDeniedHttpException('You are not the user');
        }
    }

    /**
     * @Route("/{id}", name="usuario_delete", methods={"DELETE"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function delete(Request $request, Usuario $usuario): Response
    {
        if ($this->isCsrfTokenValid('delete'.$usuario->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($usuario);
            $entityManager->flush();
        }

        return $this->redirectToRoute('usuario_index');
    }

    /**
     * @Route("/{id}/changerole", name="usuario_changerole", methods={"POST"})
     * @Security("has_role('ROLE_ADMIN')")
     * @throws \Exception
     */
    public function changeRole(Request $request, Usuario $usuario): Response
    {
        $role = $request->request->get('role');

        try
        {

            $usuario->setRole($role);

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash( 'mensaje', 'Role changed succesly!');

            return $this->redirectToRoute('usuario_index');
        }
        catch(\Exception $exception)
        {
            throw new \Exception('Something went wrong!');
        }

    }
}
