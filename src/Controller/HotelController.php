<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Hotel;
use App\Form\CommentType;
use App\Form\HotelType;
use App\Entity\Mensaje;
use App\Form\MensajeType;
use App\Repository\CommentRepository;
use App\Repository\HotelRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Helper\FileUploader;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/hotel")
 */
class HotelController extends AbstractController
{
    /**
     * @Route("/", name="hotel_index", methods={"GET"})
     * @Security("has_role('ROLE_USER')")
     */
    public function index(HotelRepository $hotelRepository): Response
    {
        $role = $this->getUser()->getRole();

        if ($role === "ROLE_ADMIN") {
            $hotels = $hotelRepository->findAll();
        } else {
            $hotels = $hotelRepository->findBy(['creator' => $this->getUser()]);
        }

        return $this->render('hotel/index.html.twig', [
            'hotels' => $hotels,
        ]);
    }

    /**
     * @Route("/new", name="hotel_new", methods={"GET","POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $hotel = new Hotel();
        $hotel->setCreator($this->getUser());
        $form = $this->createForm(HotelType::class, $hotel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $imagen */
            $imagen = $hotel->getImgName();
            $fileName = $fileUploader->upload($imagen);
            $hotel->setImgName($fileName);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($hotel);
            $entityManager->flush();

            return $this->redirectToRoute('hotel_index');
        }

        return $this->render('hotel/new.html.twig', [
            'hotel' => $hotel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/buscar", name="hotel_index_buscar", methods={"POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function buscar(Request $request, HotelRepository $hotelRepository): Response
    {
        $filterByType = $request->request->get('filterByType');
        $orderDateFrom = $request->request->get('orderDateFrom');
        $orderDateTo = $request->request->get('orderDateTo');
        $searchFilter = $request->request->get('searchFilter') ?? '';

        $hotels = $hotelRepository->findHotelesFiltrados($filterByType, $orderDateFrom, $orderDateTo, $searchFilter);

        $role = $this->getUser()->getRole();

        if ($role === "ROLE_USER") {
            $hotels = array_filter($hotels, function ($b) {
                return $b->getCreator() === $this->getUser();
            });
        }

        return $this->render('hotel/index.html.twig', [
            'hotels' => $hotels,
        ]);
    }

    /**
     * @Route("/{id}", name="hotel_show", methods={"GET", "POST"})
     */
    public function show(Request $request, Hotel $hotel): Response
    {
        $comments = $hotel->getComments();
        $bookings = $hotel->getBookings()->toArray();

        $idUser = $this->getUser() ? $this->getUser()->getId() : null;

        $reservasUsuario = array_filter($bookings, function ($b) use ($idUser) {
            return $b->getUser()->getId() === $idUser;
        });

        $reservado = $reservasUsuario ? true : false;

        $comment = new Comment();
        $comment->setHotel($hotel);
        $comment->setUser($this->getUser());
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();

            return $this->redirectToRoute('hotel_show', [
                'id' => $hotel->getId()
            ]);
        }

        return $this->render('hotel/show.html.twig', [
            'hotel' => $hotel,
            'comments' => $comments,
            'form' => $form->createView(),
            'reservado' => $reservado,
        ]);
    }

    /*
      @Route("/{id}", name="hotel_show", methods={"GET"})

    public function show(Hotel $hotel): Response
    {
        $comments = $hotel->getComments();

        return $this->render('hotel/show.html.twig', [
            'hotel' => $hotel,
            'comments' => $comments,
        ]);
    }
*/
    /**
     * @Route("/{id}/edit", name="hotel_edit", methods={"GET","POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function edit(Request $request, FileUploader $fileUploader, Hotel $hotel): Response
    {
        if (($this->getUser() === $hotel->getCreator()) || ($this->getUser()->getRole() === 'ROLE_ADMIN'))
        {
            $hotel->setImgName(new File(
                $this->getParameter('images_directory') . '/' . $hotel->getImgName()));

            $form = $this->createForm(HotelType::class, $hotel);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                /** @var UploadedFile $imagen */
                $imagen = $hotel->getImgName();
                $fileName = $fileUploader->upload($imagen);
                $hotel->setImgName($fileName);

                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('hotel_index', [
                    'id' => $hotel->getId(),
                ]);
            }

            return $this->render('hotel/edit.html.twig', [
                'hotel' => $hotel,
                'form' => $form->createView(),
            ]);
        }
        else {
            throw new AccessDeniedHttpException('You are not the creator');
        }

    }

    /**
     * @Route("/{id}", name="hotel_delete", methods={"DELETE"})
     * @Security("has_role('ROLE_USER')")
     */
    public function delete(Request $request, Hotel $hotel): Response
    {
        if ($this->isCsrfTokenValid('delete'.$hotel->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($hotel);
            $entityManager->flush();
        }

        return $this->redirectToRoute('hotel_index');
    }

    /**
     * @Route("/{id}/changestatus", name="hotel_changestatus", methods={"POST"})
     * @Security("has_role('ROLE_USER')")
     * @throws \Exception
     */
    public function changeStatus(Request $request, Hotel $hotel): Response
    {
        $status = $request->request->get('status');

        try
        {

            $hotel->setActive($status);

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash( 'mensaje', 'Status changed succesly!');

            return $this->redirectToRoute('hotel_index');
        }
        catch(\Exception $exception)
        {
            throw new \Exception('Something went wrong!');
        }

    }

}
