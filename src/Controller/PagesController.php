<?php

namespace App\Controller;

use App\Entity\Usuario;
use App\Repository\UsuarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Hotel;
use App\Form\HotelType;
use App\Repository\HotelRepository;
use Symfony\Component\HttpFoundation\Request;


class PagesController extends AbstractController
{
    /**
     * @Route("/{currentPage}", name="pages_index", requirements={"currentPage"="\d+"}, methods={"GET"})
     */
    public function index(HotelRepository $hotelRepository, $currentPage = 1): Response
    {
        $limit = 3;
        $hotelsResultado = $hotelRepository->findAllPaginados($currentPage, $limit);

        $hotels = $hotelsResultado['paginator'];
        $hotelsQueryCompleta = $hotelsResultado['query'];

        $maxPages = ceil(($hotelsResultado['paginator'])->count() / $limit);

        return $this->render('pages/index.html.twig', [
            'hotels' => $hotels,
            'maxPages' => $maxPages,
            'thisPage' => $currentPage,
            'all_items' => $hotelsQueryCompleta,
            'ruta' => 'index'
        ]);
    }

    /**
     * @Route("/about", name="pages_about", methods={"get"})
     * @Template("pages/about.html.twig")
     */
    public function about()
    {

    }

    /**
     * @Route("/contact", name="pages_contact", methods={"get"})
     * @Template("pages/contact.html.twig")
     */
    public function contact()
    {

    }

    /**
     * @Route("/buscar/", name="pages_index_buscar", methods={"POST"})
     */
    public function buscar(Request $request, HotelRepository $hotelRepository): Response
    {
        $filterByType = $request->request->get('filterByType');
        $orderDateFrom = $request->request->get('orderDateFrom');
        $orderDateTo = $request->request->get('orderDateTo');
        $searchFilter = $request->request->get('searchFilter') ?? '';


        $hotelsSinFiltrar = $hotelRepository->findHotelesFiltrados($filterByType, $orderDateFrom, $orderDateTo, $searchFilter);
        $hotels = array_filter($hotelsSinFiltrar, function($hotel) {
            return $hotel->getActive();
        });


        return $this->render('pages/index.html.twig', [
            'hotels' => $hotels,
            'ruta' => 'buscar'
        ]);
    }

    /**
     * @Route("/user/{id}/{currentPage}", name="pages_index_usuario", methods={"GET"})
     */
    public function show(Usuario $usuario, HotelRepository $hotelRepository, $currentPage = 1): Response
    {
        // $hotelsSinFiltrar = $usuario->getHotels()->toArray();

        $limit = 3;
        $hotelsResultado = $hotelRepository->findHotelesUsuario($usuario, $currentPage, $limit);

        $idUsuario = $usuario->getId();

        $hotels = $hotelsResultado['paginator'];
        $hotelsQueryCompleta = $hotelsResultado['query'];

        $maxPages = ceil(($hotelsResultado['paginator'])->count() / $limit);


        return $this->render('pages/index.html.twig', [
            'hotels' => $hotels,
            'maxPages' => $maxPages,
            'thisPage' => $currentPage,
            'all_items' => $hotelsQueryCompleta,
            'ruta' => 'show',
            'idUser' => $idUsuario
        ]);
    }

}
