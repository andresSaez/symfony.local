<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 3/02/19
 * Time: 10:24
 */

namespace App\Helper;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    private $targetDir;

    public function __construct($targetDir)
   {
       $this->targetDir = $targetDir;
   }

    public function upload(UploadedFile $file)
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $file->move($this->getTargetDir(), $fileName);
        return $fileName;
    }

    public function getTargetDir()
    {
        return $this->targetDir;
    }
}