<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MensajeRepository")
 */
class Mensaje
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="enviados")
     * @ORM\JoinColumn()
     */
    private $remitente;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="recibidos")
     * @ORM\JoinColumn()
     */
    private $destinatario;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="The subject can not be left empty")
     */
    private $titulo;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="The text can not be left empty")
     */
    private $texto;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $respuestaa;

    /**
     * @ORM\Column(type="datetime", options={"default" : "CURRENT_TIMESTAMP"})
     */
    private $date;

    /**
     * Mensaje constructor.
     */
    public function __construct()
    {
        $this->respuestaa = null;
        $this->date = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getRemitente()
    {
        return $this->remitente;
    }

    /**
     * @param mixed $remitente
     * @return Mensaje
     */
    public function setRemitente($remitente)
    {
        $this->remitente = $remitente;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDestinatario()
    {
        return $this->destinatario;
    }

    /**
     * @param mixed $destinatario
     * @return Mensaje
     */
    public function setDestinatario($destinatario)
    {
        $this->destinatario = $destinatario;
        return $this;
    }

    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getTexto(): ?string
    {
        return $this->texto;
    }

    public function setTexto(string $texto): self
    {
        $this->texto = $texto;

        return $this;
    }

    public function getRespuestaa(): ?int
    {
        return $this->respuestaa;
    }

    public function setRespuestaa(?int $respuestaa): self
    {
        $this->respuestaa = $respuestaa;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
