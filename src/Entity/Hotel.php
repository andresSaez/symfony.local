<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HotelRepository")
 */
class Hotel
{
    public const RUTA_IMGS = '/img/gallery/';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Please, choose a image")
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg" })
     */
    private $imgName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="The hotel name can not be left empty")
     */
    private $hotelName;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $stars;

    /**
     * @ORM\Column(type="boolean")
     */
    private $swimmingPool;

    /**
     * @ORM\Column(type="boolean")
     */
    private $gym;

    /**
     * @ORM\Column(type="boolean")
     */
    private $wifi;

    /**
     * @ORM\Column(type="boolean")
     */
    private $roomService;

    /**
     * @ORM\Column(type="boolean")
     */
    private $airCondition;

    /**
     * @ORM\Column(type="boolean")
     */
    private $restaurant;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="The price can not be left empty")
     * @Assert\GreaterThan(value=0, message="The price is not valid. It must be a positive number")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="hotels")
     * @ORM\JoinColumn()
     */
    private $creator;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Booking",
     *     mappedBy="hotel")
     * @var Collection $bookings
     */
    private $bookings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment",
     *     mappedBy="hotel")
     * @var Collection $comments
     */
    private $comments;

    /**
     * @ORM\Column(type="date", options={"default" : "today"})
     */
    private $date;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $active;

    /**
     * Hotel constructor.
     */
    public function __construct()
    {
        $this->stars = 0;
        $this->date = new \DateTime();
        $this->active = true;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getImgName()
    {
        return $this->imgName;
    }

    /**
     * @param mixed $imgName
     * @return Hotel
     */
    public function setImgName($imgName)
    {
        $this->imgName = $imgName;
        return $this;
    }

    public function getHotelName(): ?string
    {
        return $this->hotelName;
    }

    public function setHotelName(string $hotelName = null): self
    {
        $this->hotelName = $hotelName;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description = null): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStars(): ?int
    {
        return $this->stars;
    }

    public function setStars(int $stars): self
    {
        $this->stars = $stars;

        return $this;
    }

    public function getSwimmingPool(): ?bool
    {
        return $this->swimmingPool;
    }

    public function setSwimmingPool(bool $swimmingPool): self
    {
        $this->swimmingPool = $swimmingPool;

        return $this;
    }

    public function getGym(): ?bool
    {
        return $this->gym;
    }

    public function setGym(bool $gym): self
    {
        $this->gym = $gym;

        return $this;
    }

    public function getWifi(): ?bool
    {
        return $this->wifi;
    }

    public function setWifi(bool $wifi): self
    {
        $this->wifi = $wifi;

        return $this;
    }

    public function getRoomService(): ?bool
    {
        return $this->roomService;
    }

    public function setRoomService(bool $roomService): self
    {
        $this->roomService = $roomService;

        return $this;
    }

    public function getAirCondition(): ?bool
    {
        return $this->airCondition;
    }

    public function setAirCondition(bool $airCondition): self
    {
        $this->airCondition = $airCondition;

        return $this;
    }

    public function getRestaurant(): ?bool
    {
        return $this->restaurant;
    }

    public function setRestaurant(bool $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price = null): self
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param mixed $creator
     * @return Hotel
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
        return $this;
    }

    /**
     * @return Collection Booking
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    /**
     * @return Collection Comment
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getUrlImagen()
    {
        return self::RUTA_IMGS . $this->getImgName();
    }

    public function showStars() {
        for($i = 0; $i < $this->stars; $i++) {
            echo '<span class="fa fa-star checked"></span>';
        }
        $emptyStars = 5 - $this->stars;
        for($e = 0; $e < $emptyStars; $e++) {
            echo '<span class="fa fa-star"></span>';
        }
    }

    public function getUrlMiniatura(): string
    {
        $nombreMiniatura = 'thumb-' . $this->getImgName();
        return self::RUTA_IMGS . $nombreMiniatura;
    }
}
