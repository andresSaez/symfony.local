<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 */
class Booking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hotel", inversedBy="bookings")
     * @ORM\JoinColumn()
     */
    private $hotel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="bookings")
     * @ORM\JoinColumn()
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message="Please,choice check-in day")
     * @Assert\GreaterThanOrEqual(value="today", message="Please, check-in must be greater than or equal today")
     */
    private $checkIn;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message="Please,choice check-out day")
     * @Assert\GreaterThan(value="today", message="Please, check-out must be greater than today")
     */
    private $checkOut;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * @param mixed $hotel
     * @return Booking
     */
    public function setHotel($hotel)
    {
        $this->hotel = $hotel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Booking
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function getCheckIn(): ?\DateTimeInterface
    {
        return $this->checkIn;
    }

    public function setCheckIn(\DateTimeInterface $checkIn): self
    {
        $this->checkIn = $checkIn;

        return $this;
    }

    public function getCheckOut(): ?\DateTimeInterface
    {
        return $this->checkOut;
    }

    public function setCheckOut(\DateTimeInterface $checkOut): self
    {
        $this->checkOut = $checkOut;

        return $this;
    }

    /**
     * @Assert\IsTrue(message="Check-in and check-out can not be in the same day")
     */
    public function isCheckInAndCheckOutDiasDistintos()
    {
        return !($this->getCheckIn() === $this->getCheckOut());
    }
}
