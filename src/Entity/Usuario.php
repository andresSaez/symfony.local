<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;
use Captcha\Bundle\CaptchaBundle\Validator\Constraints as CaptchaAssert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsuarioRepository")
 */
class Usuario implements UserInterface, \Serializable
{
    public const RUTA_IMGS = '/img/profile/';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="The name can not be left empty")
     */
    private $nameuser;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="The surname can not be left empty")
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="The province can not be left empty")
     */
    private $province;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Please, choose a image")
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg" })
     */
    private $avatar;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="The email can not be left empty")
     * @Assert\Email(
     *     message = "The email is not a valid email.",
     *     checkMX = true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="The password can not be left empty")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, options={"default" : "ROLE_USER"})
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=255, options={"default" : "español"})
     */
    private $language;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Mensaje",
     *     mappedBy="destinatario")
     * @var Collection $recibidos
     */
    private $recibidos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Mensaje",
     *     mappedBy="remitente")
     * @var Collection $enviados
     */
    private $enviados;

    /**
     * @var string
     * @Assert\NotBlank(message="The email can not be left empty")
     *
     */
    private $email2;

    /**
     * @var string
     * @Assert\NotBlank(message="The password can not be left empty")
     */
    private $password2;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Hotel",
     *     mappedBy="creator")
     * @var Collection $hotels
     */
    private $hotels;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Booking",
     *     mappedBy="user")
     * @var Collection $bookings
     */
    private $bookings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment",
     *     mappedBy="user")
     * @var Collection $comments
     */
    private $comments;

    /**
     * @CaptchaAssert\ValidCaptcha(
     *      message = "CAPTCHA validation failed, try again."
     * )
     */
    protected $captchaCode;

    public function getCaptchaCode()
    {
        return $this->captchaCode;
    }

    public function setCaptchaCode($captchaCode)
    {
        $this->captchaCode = $captchaCode;
    }

    /**
     * Usuario constructor.
     */
    public function __construct()
    {
        $this->role = "ROLE_USER";
        $this->language = "español";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameuser(): ?string
    {
        return $this->nameuser;
    }

    public function setNameuser(string $nameuser = null): self
    {
        $this->nameuser = $nameuser;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname = null): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getProvince(): ?string
    {
        return $this->province;
    }

    public function setProvince(string $province = null): self
    {
        $this->province = $province;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
       return $this->avatar;
    }

    /**
     * @param mixed $avatar
     * @return Usuario
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email = null): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password = null): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role="ROLE_USER"): self
    {
        $this->role = $role;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language="español"): self
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail2(): ?string
    {
        return $this->email2;
    }

    /**
     * @param string $email2
     * @return Usuario
     */
    public function setEmail2(string $email2): Usuario
    {
        $this->email2 = $email2;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword2(): ?string
    {
        return $this->password2;
    }

    /**
     * @param string $password2
     * @return Usuario
     */
    public function setPassword2(string $password2): Usuario
    {
        $this->password2 = $password2;
        return $this;
    }

    /**
     * @return Collection Hotel
     */
    public function getHotels(): Collection
    {
        return $this->hotels;
    }

    /**
     * @return Collection Booking
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    /**
     * @return Collection Comment
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    /**
     * @return Collection Mensaje
     */
    public function getRecibidos(): Collection
    {
        return $this->recibidos;
    }

    /**
     * @return Collection Mensaje
     */
    public function getEnviados(): Collection
    {
        return $this->enviados;
    }

    public function getUrlImagen()
    {
        return self::RUTA_IMGS . $this->getAvatar();
    }

    /**
     * @Assert\IsTrue(message="The emails do not match")
     */
    public function isEmailsIguales()
    {
        return ($this->getEmail() === $this->getEmail2());
    }

    /**
     * @Assert\IsTrue(message="The passwords do not match")
     */
    public function isPasswordsIguales()
    {
        return ($this->getPassword() === $this->getPassword2());
    }

    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->nameuser,
            $this->surname,
            $this->province,
            $this->email,
            $this->password,
            $this->role,
            $this->language
        ]);
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->nameuser,
            $this->surname,
            $this->province,
            $this->email,
            $this->password,
            $this->role,
            $this->language
            ) = unserialize($serialized);
    }

    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return array('ROLE_USER');
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return [ $this->getRole() ];
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
