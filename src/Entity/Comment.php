<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 */
class Comment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="comments")
     * @ORM\JoinColumn()
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hotel", inversedBy="comments")
     * @ORM\JoinColumn()
     */
    private $hotel;

    /**
     * @ORM\Column(type="string", length=1000)
     * @Assert\NotBlank(message="The text can not be left empty")
     */
    private $text;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="The valuation can not be left empty")
     * @Assert\Range(
     *      min = 1,
     *      max = 5,
     *      minMessage = "The valuation should be between 1 and 5",
     *      maxMessage = "The valuation should be between 1 and 5")
     */
    private $stars;

    /**
     * @ORM\Column(type="datetime", options={"default" : "CURRENT_TIMESTAMP"})
     */
    private $date;

    /**
     * Comment constructor.
     */
    public function __construct()
    {
        $this->date = new \DateTime();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Comment
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * @param mixed $hotel
     * @return Comment
     */
    public function setHotel($hotel)
    {
        $this->hotel = $hotel;
        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getStars(): ?int
    {
        return $this->stars;
    }

    public function setStars(int $stars): self
    {
        $this->stars = $stars;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function showStars() {
        for($i = 0; $i < $this->stars; $i++) {
            echo '<span class="fa fa-star checked"></span>';
        }
    }
}
