<?php

namespace App\Repository;

use App\Entity\Hotel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Hotel|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hotel|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hotel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HotelRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Hotel::class);
    }

    public function findAll()
    {
        $qb = $this->createQueryBuilder('hotel')
            ->innerJoin('hotel.creator', 'usuario')
            ->orderBy('hotel.date', 'DESC');

        $query = $qb->getQuery();

        return $query->execute();
        // return $this->findBy([], ['date' => 'DESC']);
    }

    public function findHotelesFiltrados($filterByType, $orderDateFrom = null, $orderDateTo = null, $searchFilter = null)
    {
        $qb = $this->createQueryBuilder('h');

        if ($filterByType != 'all') {
            switch ($filterByType)
            {
                case 'swimmingPool':
                    $qb ->andWhere('h.swimmingPool = 1');
                    break;
                case 'gym':
                    $qb ->andWhere('h.gym = 1');
                    break;
                case 'wifi':
                    $qb ->andWhere('h.wifi = 1');
                    break;
                case 'roomService':
                    $qb ->andWhere('h.roomService = 1');
                    break;
                case 'airCondition':
                    $qb ->andWhere('h.airCondition = 1');
                    break;
                case 'restaurant':
                    $qb ->andWhere('h.restaurant = 1');
                    break;
            }
        }

        if ($orderDateFrom && $orderDateTo) {
            $qb ->andWhere($qb->expr()->between('h.date', ':orderDateFrom', ':orderDateTo'))
                ->setParameter('orderDateFrom', $orderDateFrom)
                ->setParameter('orderDateTo', $orderDateTo);
        } else if ($orderDateFrom) {
            $qb ->andWhere($qb->expr()->gte('h.date', ':orderDateFrom'))
                ->setParameter('orderDateFrom', $orderDateFrom);
        } else if ($orderDateTo) {
            $qb ->andWhere($qb->expr()->lte('h.date', ':orderDateTo'))
                ->setParameter('orderDateTo', $orderDateTo);
        }

        if ($searchFilter) {
            $qb ->andWhere($qb->expr()->like('h.hotelName',':searchFilter'))
                ->setParameter('searchFilter', '%' . $searchFilter . '%');
        }

        $qb->orderBy('h.date', 'DESC');
        $query = $qb->getQuery();

        return $query->execute();
    }

    public function paginate($dql, $page = 1, $limit = 3)
    {
        $paginator = new Paginator($dql);

        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1)) // Offset
            ->setMaxResults($limit); // Limit

        return $paginator;
    }

    public function findAllPaginados($currentPage =1, $limit = 3)
    {
        $qb = $this->createQueryBuilder('hotel')
            ->andWhere('hotel.active = 1')
            ->innerJoin('hotel.creator', 'usuario')
            ->orderBy('hotel.date', 'DESC');

        $query = $qb->getQuery();

        $paginator = $this->paginate($query, $currentPage, $limit);

        return array('paginator' => $paginator, 'query' => $query);
    }

    public function findHotelesUsuario($creator, $currentPage =1, $limit = 3)
    {
        $qb = $this->createQueryBuilder('hotel')
            ->andWhere('hotel.active = 1')
            ->andWhere('hotel.creator = :creator')
            ->setParameter('creator', $creator)
            ->innerJoin('hotel.creator', 'usuario')
            ->orderBy('hotel.date', 'DESC');

        $query = $qb->getQuery();

        $paginator = $this->paginate($query, $currentPage, $limit);

        return array('paginator' => $paginator, 'query' => $query);
    }

    public function findHotelesFiltradosPaginados($filterByType, $orderDateFrom = null, $orderDateTo = null, $searchFilter = null, $currentPage = 1, $limit = 3)
    {
        $qb = $this->createQueryBuilder('h');

        if ($filterByType != 'all') {
            switch ($filterByType)
            {
                case 'swimmingPool':
                    $qb ->andWhere('h.swimmingPool = 1');
                    break;
                case 'gym':
                    $qb ->andWhere('h.gym = 1');
                    break;
                case 'wifi':
                    $qb ->andWhere('h.wifi = 1');
                    break;
                case 'roomService':
                    $qb ->andWhere('h.roomService = 1');
                    break;
                case 'airCondition':
                    $qb ->andWhere('h.airCondition = 1');
                    break;
                case 'restaurant':
                    $qb ->andWhere('h.restaurant = 1');
                    break;
            }
        }

        if ($orderDateFrom && $orderDateTo) {
            $qb ->andWhere($qb->expr()->between('h.date', ':orderDateFrom', ':orderDateTo'))
                ->setParameter('orderDateFrom', $orderDateFrom)
                ->setParameter('orderDateTo', $orderDateTo);
        } else if ($orderDateFrom) {
            $qb ->andWhere($qb->expr()->gte('h.date', ':orderDateFrom'))
                ->setParameter('orderDateFrom', $orderDateFrom);
        } else if ($orderDateTo) {
            $qb ->andWhere($qb->expr()->lte('h.date', ':orderDateTo'))
                ->setParameter('orderDateTo', $orderDateTo);
        }

        if ($searchFilter) {
            $qb ->andWhere($qb->expr()->like('h.hotelName',':searchFilter'))
                ->setParameter('searchFilter', '%' . $searchFilter . '%');
        }

        $qb->andWhere('h.active = 1');

        $qb->orderBy('h.date', 'DESC');
        $query = $qb->getQuery();

        $paginator = $this->paginate($query, $currentPage, $limit);

        return array('paginator' => $paginator, 'query' => $query);
    }

    // /**
    //  * @return Hotel[] Returns an array of Hotel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Hotel
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
