<?php

namespace App\Form;

use App\Entity\Usuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Captcha\Bundle\CaptchaBundle\Form\Type\CaptchaType;


class UsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nameuser')
            ->add('surname')
            ->add('province')
            ->add('avatar', FileType::class, ['label' => 'Image (PNG/JPG)'])
            ->add('email', EmailType::class)
            ->add('email2', EmailType::class)
            ->add('password', PasswordType::class)
            ->add('password2',PasswordType::class)
            ->add('captchaCode', CaptchaType::class, ['captchaConfig' => 'RegisterCaptcha', 'label' => 'Retype the characters from the picture'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Usuario::class,
        ]);
    }
}
